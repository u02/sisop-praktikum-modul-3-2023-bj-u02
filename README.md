<h1>Praktikum Modul 3 Sistem Operasi</h1>
<h3>Group U02</h3>

| NAME                      | NRP       |
|---------------------------|-----------|
|Pascal Roger Junior Tauran |5025211072 |
|Riski Ilyas                |5025211189 |
|Armstrong Roosevelt Zamzami|5025211191 |


## Number 1
```
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!
```
### 1A

```
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.


```
Solution:<br>
```c

int main()
{
    int shmid, i;
    key_t key;
    char *shm, *s, buf[MAX_SIZE];

    // Create shared memory segment
    key = ftok(".", 'a');
    shmid = shmget(key, MAX_SIZE, IPC_CREAT | 0666);
    if(shmid == -1){
        perror("shmget");
        exit(1);
    }

    // Attach shared memory segment to process
    shm = shmat(shmid, NULL, 0);
    if(shm == (char*) -1){
        perror("shmat");
        exit(1);
    }

    // Read file.txt and count character frequency
    FILE *fp = fopen("file.txt", "r");
    int freq[26] = {0};
    char c;
    while((c = fgetc(fp)) != EOF){
        if(c >= 'a' && c <= 'z') freq[c - 'a']++;
        else if(c >= 'A' && c <= 'Z') freq[c - 'A']++;
    }
    fclose(fp);

    // Write character frequency to shared memory
    s = shm;
    for(i=0; i<26; i++) *s++ = freq[i];
    *s = '\0';


```
Exlpanation:
- This code creates a shared memory segment using System V IPC mechanism, attaches it to the current process, and then reads a file named "file.txt" to count the frequency of each alphabetic character. The frequency of each character is stored in an integer array called `freq` of size 26, where `freq[0]` represents the frequency of the letter 'a', `freq[1]` represents the frequency of the letter 'b', and so on. 
- After counting the character frequency, the code writes the frequency array to the shared memory segment. This is done by initializing the `s` pointer to point to the starting address of the shared memory segment, and then using a loop to write the frequency of each character to the shared memory segment. 
- The loop increments the `s` pointer after writing each frequency value, so that the next value can be written to the next memory location in the shared memory segment. Finally, the code terminates the string in the shared memory segment by writing a null character `'\0'` to the next memory location after the last frequency value. 
- Note that the `MAX_SIZE` constant is used to specify the size of the shared memory segment and must be defined before this code is compiled. The code also performs error checking for the System V IPC calls using `perror()` and `exit()` in case the calls fail.

### 1B
```
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

```
Solution:<br>
```c
 // Create child process
    pid_t pid = fork();
    if(pid == -1){
        perror("fork");
        exit(1);
    }
    else if(pid == 0) // Child process
    {
        printf("Child process executing...\n");
        // Attach shared memory segment to child process
        char *shm_child = shmat(shmid, NULL, 0);
        if(shm_child == (char*) -1){
            perror("shmat");
            exit(1);
        }

        // Read character frequency from shared memory and print result
        s = shm_child;
        printf("Character frequency:\n");
        int word[26] = {0}, size = 0;
        for(i=0; i<26; i++){
            word[i] = *s;
            if(word[i]>0) size++;
            printf("%c: %d\n", 'a'+i, *s++);
        }

        printf("\nCompressed Code:\n");
        HuffmanCodes(word, size);

        // Detach shared memory segment from child process
        if(shmdt(shm_child) == -1) {
            perror("shmdt");
            exit(1);
        }
        exit(0);
    }
    else // Parent process
    {
        printf("Parent process executing...\n");

        // Wait for child process to finish
        wait(NULL);

        // Detach shared memory segment from parent process
        if(shmdt(shm) == -1) {
            perror("shmdt");
            exit(1);
        }

        // Deallocate shared memory segment
        if(shmctl(shmid, IPC_RMID, NULL) == -1) {
            perror("shmctl");
            exit(1);
        }
    }


```
Explanation:
- This code creates a child process using the `fork()` system call. The parent process creates the shared memory segment and writes the character frequency data to the segment. The child process then attaches to the shared memory segment, reads the character frequency data, compresses it using the Huffman coding algorithm, and detaches from the shared memory segment. 
- The parent process waits for the child process to finish and then detaches from the shared memory segment. Finally, the parent process deallocates the shared memory segment using the `shmctl()` system call. If the `fork()` call fails, an error message is printed using `perror()` and the program exits. 
- If the child process successfully attaches to the shared memory segment, it reads the character frequency data from the segment and prints it to the console, as well as the compressed code using the Huffman algorithm. The child process then detaches from the shared memory segment and exits with status 0.
- If the parent process successfully detaches from the shared memory segment, it deallocates the shared memory segment using `shmctl()`. If any of these system calls fail, an error message is printed using `perror()` and the program exits with status 1.

##  Number2
```
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.
```
### 2A
```
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

```
Solution:<br>
```c
int main() {
    key_t key = 2003;
    int mat1[ROW1][COL1], mat2[ROW2][COL2], i, j, k, SHMID;
    ll *result;

    // Inisialisasi untuk random seed
    srand(time(NULL));

    // Isi matriks 1 dengan angka antara 1 sampai dengan 5
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL1; j++) mat1[i][j] = rand() % 5 + 1;
    }

    // Isi matriks 1 dengan angka antara 1 sampai dengan 4
    for(i=0; i<ROW2; i++){
        for(j=0; j<COL2; j++) mat2[i][j] = rand() % 4 + 1;
    }

    // Buat Shared Memory untuk hasil perkalian Matriks 1 & 2
    SHMID = shmget(key, sizeof(ll) * ROW1 * COL2, IPC_CREAT | 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }

    // Masukkan segmen dari shared memory ke process
    result = (ll*) shmat(SHMID, NULL, 0);
    if(result == (ll*) -1){
        perror("shmat");
        exit(1);
    }

    // Perkalian matriks sekaligus mencetak hasilnya
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL2; j++){
            result[i*COL2 + j] = 0;
            for(k=0; k<ROW2; k++) result[i*COL2 + j] += mat1[i][k] * mat2[k][j];
            printf("%llu ", result[i*COL2 + j]);
        }
        printf("\n");
    }

    // Melepas shared memory dari process
    if(shmdt(result) == -1){
        perror("shmdt");
        exit(1);
    }

    printf("Result matrix is sent to shared memory.\n");

    return 0;
}


```
Explanation:
- 'srand' function is called with the argument 'time(NULL)' to initialize the random number generator with the current time as the seed.
- The nested loop is used to fill mat1 with random numbers between 1 and 5. The rand function is called, and the result is modulo divided by 5 and then incremented by 1 to get the desired range.
- Another nested loop is used to fill 'mat2' with random numbers between 1 and 4.
- The 'shmget' function is called to create a shared memory segment with the specified key.The flags 'IPC_CREAT' and '0666' are used to create the segment and set its permissions. The function returns the shared memory identifier (SHMID), and The 'shmat' function is called to attach the shared memory segment to the process. The 'SHMID' is passed as the identifier
-If the 'shmat' function fails and returns -1, an error message is printed using perror and the program exits with a status of 1. Three nested loops are used to perform matrix multiplication. The result of the multiplication is stored in the shared memory segment pointed to by result
- After the matrix multiplication and printing the result, the 'shmdt' function is called to detach the shared memory segment from the process. The result pointer is passed as the argument. If the function fails and returns -1, an error message is printed using 'perror', and the program exits with a status of 1.
### 2B
```
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory).
```
Solution:<br>
```c
ll findFactorial(ll n){
    if(n == 0) return 1;
    else return n * findFactorial(n-1);
}

void *calculateFactorial(void *args){
    int (*index) = args;
    RESULT_MATRIX[*index] = findFactorial(RESULT_MATRIX[*index]); 

    pthread_exit(NULL);
}

int main()
{
    pthread_t TID[100];
    key_t KEY = 2003;
    int SHMID, i, j;

    // Mendapatkan segment shared memory untuk hasil perkalian matriks
    SHMID = shmget(KEY, sizeof(ll)*ROW*COL, 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }


    // Masukkan segment shared memory ke process
    RESULT_MATRIX = (ll*) shmat(SHMID, NULL, 0);
    if(RESULT_MATRIX == (ll*) -1){
        perror("shmat");
        exit(1);
    }


    // Cetak matriks terlebih dahulu
    printf("Result Matrix:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Menghitung Faktorial dengan Thread Baru
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) {
            int *index = (int*)malloc(sizeof(int));
            *index = i*COL + j;
            pthread_create(&TID[i*COL +j], NULL, calculateFactorial, (void*)index);
        }
        printf("\n");
    }

    // Menunggu Thread selesai
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) {
           if(pthread_join(TID[i*COL +j], NULL)){
            perror("pthread_join");
            exit(1);
            }
        }
    }
    printf("\n");


    // Cetaks Matriks Hasil Faktorial
    printf("Result Matrix Factorial:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Lepas segment shared memory dari process
    if(shmdt(RESULT_MATRIX) == -1){
        perror("shmdt");
        exit(1);
    }


    // Dealokasikan segment shared memory
    if(shmctl(SHMID, IPC_RMID, NULL) == -1){
        perror("shmctl");
        exit(1);
    }

    return 0;
}

```
Explanation:
- findFactorial()' function is defined. It calculates the factorial of a given number n using the formula, If n is 0, it returns 1 as the base case.
- The 'calculateFactorial()' function is defined to be executed by each thread. It receives a void pointer argument args, that will be casted to an int* pointer named 'index'. The function calculates the factorial of the element in the 'RESULT_MATRIX' array at the index specified by index and stores the result back into the same location.
- The code retrieves the shared memory segment for the matrix multiplication result using the 'shmget()' function. It specifies the key, the size, and the flag '0666' for read or write permissions. The result of the shared memory identifier is stored in the 'SHMID' variable.
- The 'shmat()' function attaches the shared memory segment to the current process using the shared memory identifier. It returns a pointer to the shared memory segment and stored in the 'RESULT_MATRIX' variable.
- Before calculating the factorial The code prints the contents of the result matrix 'RESULT_MATRIX'
- create a thread for each element of the result matrix. It passes the element's index as an argument to the 'calculateFactorial()' function using 'pthread_create()'.
- After creating the threads, the code waits for each thread to finish using 'pthread_join()'.
- The code prints the contents of the result matrix
- The shared memory segment is detached from the process using 'shmdt''RESULT_MATRIX'.
### 2C
```
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

```
Solution:<br>
```c
ll findFactorial(ll n){
    if(n == 0) return 1;
    else return n * findFactorial(n-1);
}

void *calculateFactorial(void *args){
    ll (*mat) = args;
    int i, j;
    for(j=0; j<COL; j++){
        for(i=0; i<ROW; i++){
            ll fac = findFactorial(mat[i*COL + j]);
            mat[i*COL + j] = fac;
        }
    }

    pthread_exit(NULL);
}

int main()
{
    pthread_t TID;
    key_t KEY = 2003;
    int SHMID, i, j;
    ll *RESULT_MATRIX;

    // Mendapatkan segment shared memory untuk hasil perkalian matriks
    SHMID = shmget(KEY, sizeof(ll)*ROW*COL, 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }

    // Cetaks Matriks Hasil Faktorial
    printf("Result Matrix Factorial:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


```

Explanation:
- The 'findFactorial' function is defined to calculate the factorial of a given number recursively.
- The 'calculateFactorial' function is defined, which is the function executed by the thread. It takes a 'void*' argument, which is expected to be a pointer to the result matrix. Inside the function, the factorial of each element of the matrix is calculated and updated in place.
- The 'pthread_create' function is called to create a new thread. The TID variable is passed to store the thread ID, NULL is passed as the thread attributes, the 'calculateFactorial' function is passed as the thread function, and 'RESULT_MATRIX' is passed as the argument.
- If the 'pthread_create' function fails and returns a non-zero value, an error message is printed using perror, and the program exits.
- The result matrix after calculating the factorial is printed using nested loops.

### 2D
```
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

```
Solution:<br>
```c
// Function to calculate factorial of a number
ll findFactorial(ll n){
    if(n == 0) return 1;
    else return n * findFactorial(n-1);
}

// Function to calculate factorial for each element in a row
void calculateFactorial(){
    int i, j;
    for(j=0; j<COL; j++){
        for(i=0; i<ROW; i++){
            ll fac = findFactorial(RESULT_MATRIX[i*COL + j]);
            RESULT_MATRIX[i*COL + j] = fac;
        }
    }
}

int main()
{
    key_t KEY = 2003;
    int SHMID, i, j;

    // Mendapatkan segment shared memory untuk hasil perkalian matriks
    SHMID = shmget(KEY, sizeof(ll)*ROW*COL, 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }


    // Masukkan segment shared memory ke process
    RESULT_MATRIX = (ll*) shmat(SHMID, NULL, 0);
    if(RESULT_MATRIX == (ll*) -1){
        perror("shmat");
        exit(1);
    }


    // Cetak matriks terlebih dahulu
    printf("Result Matrix:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Cukup memanggil Function tanpa Thread Baru
    calculateFactorial();


    // Cetaks Matriks Hasil Faktorial
    printf("Result Matrix Factorial:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Lepas segment shared memory dari process
    if(shmdt(RESULT_MATRIX) == -1){
        perror("shmdt");
        exit(1);
    }


    // Dealokasikan segment shared memory
    if(shmctl(SHMID, IPC_RMID, NULL) == -1){
        perror("shmctl");
        exit(1);
    }

    return 0;
}
```

Explanation:
- It's the same code from the previous code, but the difference is that we don't use thread and just call the function using  'calculateFactorial()'
- In cinta.c, each element of the result matrix is processed by a separate thread using the calculateFactorial() function. The code dynamically creates threads for each element, assigns the index to each thread, and calculates the factorial of the corresponding element within the thread. This method allows factorial calculations using multiple threads.
- In sisop.c It directly calls the calculateFactorial() function without creating new threads. Inside the calculateFactorial() function, it loop over each element of the result matrix and calculates the factorial one by one. The code will count the factorial calculations without using multithreading.

## Number3
```
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.
```
## 3A
```
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.
```
Solution:<br>

```c
//No current solution
```

Explanation:
- `No current explanation` 


## 3B
```
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
```
Solution:<br>

```c
//No current solution
```

Explanation:<br>
- `No current explanation`
### 3C
```
Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
```
Solution:<br>

```c
//No current solution
```
Explanation:<br>
- `No current explanation`

### 3D
```
User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`

### 3E
```
User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`

### 3F
```
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`

### 3G
```
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`

## Number4
```
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut!
```

### 4A

```
Download dan unzip file tersebut dalam kode c bernama unzip.c.

```
Solution:<br>
```c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<dirent.h>
#include<pthread.h>
//#define URL "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download"

void *download(void *arg) {
    int result = system("/usr/bin/wget -q -O hehe https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
    if (result != 0) {
        printf("Error: Failed to download the file. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }
    pthread_exit(NULL);
}

void *extract(void *arg) {
    int result = system("/usr/bin/unzip hehe.zip");
    if (result != 0) {
        printf("Error: Failed to extract the file. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }
    pthread_exit(NULL);
}

int main(){
    pthread_t downloadThread, extractThread;
    int downloadResult, extractResult;

    downloadResult = pthread_create(&downloadThread, NULL, download, NULL);
    if (downloadResult != 0) {
        printf("Error: Failed to create download thread. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }

    extractResult = pthread_create(&extractThread, NULL, extract, NULL);
    if (extractResult != 0) {
        printf("Error: Failed to create extract thread. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }

    pthread_join(downloadThread, NULL);
    pthread_join(extractThread, NULL);

    return 0;
}
```
Exlpanation:
- `*download()` is the function in which will be called download the zip file.
- `wget` this command is based on the `system` function which are passed to `wget` and `q` to supress the outpout of the command and `-o` which specifies the output name which in this case is `hehe`. The download url is psecified using the `URL` macro and the function call is stored in the variable `result`. 
- `*extract` is a function that will be used to extract the zip file.
- `system("/usr/bin/unzip hehe.zip")` uses the `system` function to execute the `unzip` command on `hehe.zip`
- `pthread_t downloadThread, extractThread` declares two pthread_t variables which will hold the thread IDs of the two threads that will be created. 
- `downloadResult` creates a new thread using the pthread_create fucntion. Which points out to `pthread_t` variable, which holds the thread ID of the new thread. It then points out the third arguement to the `download` function that is made earlier.
- `extractResult` this is similar to the previous command but it creates a new thread to the extract function instead.
- `pthread_join(downloadThread, NULL);` and `pthread_join(extractThread, NULL);` wait for the two threads created to complete by calling the `pthread_join` function. 

### 4B
```
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
```
Solution:<br>
```c
// Function to read the extensions from extensions.txt and store them in an array
int readExtensions(char extensions[][256]) {
    FILE *fp = fopen("extensions.txt", "r");
    if (fp == NULL) {
        perror("fopen error");
        exit(EXIT_FAILURE);
    }

    int i = 0;
    while (fgets(extensions[i], 256, fp)) {
        // Remove the newline character from the end of the string
        extensions[i][strcspn(extensions[i], "\n")] = 0;
        printf("%s\n", extensions[i]);
        i++;
    }

    fclose(fp);
    return i;
}

// Function to check if a file has a specified extension
int hasExtension(char *filename, char *extension) {
    char *dot = strrchr(filename, '.');
    if (dot && !strcmp(dot + 1, extension)) {
        printf("file found\n");
        return 1;
    }
    return 0;
}

// Function to sort a file based on its extension
void *sortFile(void *arg) {
    File *file = (File *) arg;

    char dirName[256];
    snprintf(dirName, sizeof(dirName), "./%s", file->extension);

    // Create the directory if it doesn't exist
    if (access(dirName, F_OK) == -1) {
        mkdir(dirName, 0777);
    }

    // Move the file to the appropriate directory
    char command[512];
    snprintf(command, sizeof(command), "mv files/%s %s/%s", file->filename, dirName, file->filename);
    system(command);

    free(file);
    pthread_exit(NULL);
}

int main() {
    // Read the extensions from extensions.txt
    char extensions[256][256];
    readExtensions(extensions);

    int numExtensions = readExtensions(extensions);
    countExtensions(extensions, numExtensions);

    // Open the directory containing the unsorted files
    DIR *dir = opendir("files");
    if (dir == NULL) {
        perror("opendir error");
        exit(EXIT_FAILURE);
    }

    // Create the threads to sort the files
    pthread_t thread_id[1024];
    int thread_count = 0;
    //int counter = 0;

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char *filename = entry->d_name;
            for (int i = 0; i < 256; i++) {
                if (hasExtension(filename, extensions[i])) {
                    // Create a File struct to hold the file name and its extension
                    File *file = malloc(sizeof(File));
                    strcpy(file->filename, filename);
                    strcpy(file->extension, extensions[i]);
                    //counter++;

                    // Create a thread to sort the file
                    pthread_create(&thread_id[thread_count], NULL, sortFile, file);
                    thread_count++;

                    break;
                }
                //printf("%d\n", counter);
                //counter = 0;
            }
        }
    }

    // Wait for all threads to finish
    for (int i = 0; i < thread_count; i++) {
        pthread_join(thread_id[i], NULL);
    }
    // Close the directory
    closedir(dir);

    return 0;
}
```
Explanation:
- `readExtensions()` is the function to open and read extensions.txt and append the extensions into the aaray `extensions`.
- `hasExtension()` checks the folder for extensions that matches the extensions in the `extensions` array.
- `sortFile()` sorts the files using `snprintf`to make a `system` command to move the files to their appropriate directory and if no such directory exists, it will make the directory with `mkdir` command and name them with the extensions in the `extensions` array.
- `main()` in this fucntion, the `readExtensions`, `hasExtension` and `sortFile` functions will be called in order to sort the files and declares threads to sort the file with `pthread_create`.
- `strcpy(file->extension, extensions[i])` copies the extension to the `extension` field of the `file` structure.
- `pthread_join(thread_id[i], NULL);` waits for each thread to finish using the `pthread_join` function.

### 4C
```
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
```
Solution:<br>
```c
void countExtensions(char extensions[][256], int numExtensions) {
    DIR *dir = opendir("files");
    if (dir == NULL) {
        perror("opendir error");
        exit(EXIT_FAILURE);
    }

    int counts[numExtensions + 1]; // +1 for "other" category
    for (int i = 0; i <= numExtensions; i++) {
        counts[i] = 0;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char *filename = entry->d_name;
            int foundExtension = 0;
            for (int i = 0; i < numExtensions; i++) {
                if (hasExtension(filename, extensions[i])) {
                    foundExtension = 1;
                    counts[i]++;
                    break;
                }
            }
            if (!foundExtension) {
                counts[numExtensions]++;
            }
        }
    }

    closedir(dir);

    printf("File Extension Counts:\n");
    for (int i = 0; i < numExtensions; i++) {
        printf("%s: %d\n", extensions[i], counts[i]);
    }
    printf("Other: %d\n", counts[numExtensions]);
}

```
Explanation:
- `countExtensions()` is the function to count the number of extensions that are present in the file folder. This is done 
- `counts` is an integer array with an initial value of 0.
- In the while function, the program will loop through the extensions in the folder and call the fucntion `hasExtension` and if a mathching extension is found, the corresponding element i nthe count array is incremented. If the file does not correspond, the other element will be incremented.
- The program then prints out the extension type along with its element that corresponds to the number of occurences it has in the file folder.

### 4D
```
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

```
Explanation:
- `pthread_create` was used whenever the program wants to do actions like sort the files, and etc.
- `pthread_t thread_id[1024];` was used to declare an array of thread IDs to hold the IDs of the threads that will be created.
- `pthread_join(thread_id[i], NULL);` was used to wait for each thread to finish using the `pthread_join` function.

### 4E
```
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
```
Solution:<br>
```c
void log_event(char *event_type, char *extension, char *src_path, char *dest_path) {
    time_t now;
    struct tm *local_time;
    char timestamp[20];
    
    now = time(NULL);
    local_time = localtime(&now);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", local_time);
    
    if (strcmp(event_type, "ACCESS") == 0) {
        printf("%s %s %s\n", timestamp, "ACCESSED", src_path);
    } else if (strcmp(event_type, "MOVE") == 0) {
        printf("%s %s %s file : %s > %s\n", timestamp, "MOVED", extension, src_path, dest_path);
    } else if (strcmp(event_type, "MAKE") == 0) {
        printf("%s %s %s\n", timestamp, "MADE", dest_path);
    } else {
        printf("Invalid event type\n");
    }
}

```
Explanation:
- `log_event()` is the function to print out an event log that shows a log of folder access.
- `struct tm *local_time;` is used to represent the date and time, and the `localtime(&now)` value is appended to the struct `local_time` with the format `%d-%m-%Y %H:%M:%S`.  
- `event_type` along with its event type `ACCESS`, `MOVE`, and `MAKE` dictates the log message that will show the time, along with `event_type` and the `src_path`.

### 4F
```
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `no current explanation`

