#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include<sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#define URL "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download"
#define ZIP_FILENAME "hehe.zip"

void *download(void *arg) {
    int result = system("/usr/bin/wget -q -O hehe https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
    if (result != 0) {
        printf("Error: Failed to download the file. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }
    pthread_exit(NULL);
}

void *extract(void *arg) {
    int result = system("/usr/bin/unzip hehe.zip");
    if (result != 0) {
        printf("Error: Failed to extract the file. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }
    pthread_exit(NULL);
}

int main(){
    pthread_t downloadThread, extractThread;
    int downloadResult, extractResult;

    downloadResult = pthread_create(&downloadThread, NULL, download, NULL);
    if (downloadResult != 0) {
        printf("Error: Failed to create download thread. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }

    extractResult = pthread_create(&extractThread, NULL, extract, NULL);
    if (extractResult != 0) {
        printf("Error: Failed to create extract thread. errno=%d\n", errno);
        exit(EXIT_FAILURE);
    }

    pthread_join(downloadThread, NULL);
    pthread_join(extractThread, NULL);

    return 0;
}
