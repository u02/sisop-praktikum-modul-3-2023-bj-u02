#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>
#define MAX_SIZE 1024
#define MAX_TREE_HT 100

struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
};

struct MinHeap {
    unsigned size;
    unsigned capacity;
    struct MinHeapNode **array;
};

struct MinHeapNode *newNode(char data, unsigned freq) {
    struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}

struct MinHeap *createMinHeap(unsigned capacity) {
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
    return minHeap;
}

void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
    struct MinHeapNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx) {
    int smallest = idx, left = 2 * idx + 1, right = 2 * idx + 2;
    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq) smallest = left;
    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq) smallest = right;
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int isSizeOne(struct MinHeap *minHeap) { return (minHeap->size == 1); }

struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
    struct MinHeapNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
    int n = minHeap->size - 1, i;
    for (i = (n - 1) / 2; i >= 0; --i) minHeapify(minHeap, i);
}

void printArr(int arr[], int n) {
    int i;
    for (i = 0; i < n; ++i) printf("%d", arr[i]);
    printf("\n");
}

int isLeaf(struct MinHeapNode *root) { return !(root->left) && !(root->right); }

void printCodes(struct MinHeapNode *root, int arr[], int top) {
    if (root->left){
        arr[top] = 0;
        printCodes(root->left, arr, top + 1);
    }
    if (root->right){
        arr[top] = 1;
        printCodes(root->right, arr, top + 1);
    }
    if (isLeaf(root)){
        printf("%c: ", root->data);
        printArr(arr, top);
    }
}

void HuffmanCodes(int freq[], int size) {
    struct MinHeapNode *left, *right, *top;
    struct MinHeap *minHeap = createMinHeap(size);
    int ctr=0;
    for (int i = 0; i < 26; ++i){
        if(freq[i]==0) continue;
        minHeap->array[ctr++] = newNode((char)(i+'a'), freq[i]);
    }
        
    minHeap->size = size;
    buildMinHeap(minHeap);

    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);
        top = newNode('$', left->freq + right->freq);
        top->left = left;
        top->right = right;
        insertMinHeap(minHeap, top);
    }

    int arr[MAX_TREE_HT], mTop = 0;
    printCodes(extractMin(minHeap), arr, mTop);
}

int main()
{
    int shmid, i;
    key_t key;
    char *shm, *s, buf[MAX_SIZE];

    // Create shared memory segment
    key = ftok(".", 'a');
    shmid = shmget(key, MAX_SIZE, IPC_CREAT | 0666);
    if(shmid == -1){
        perror("shmget");
        exit(1);
    }

    // Attach shared memory segment to process
    shm = shmat(shmid, NULL, 0);
    if(shm == (char*) -1){
        perror("shmat");
        exit(1);
    }

    // Read file.txt and count character frequency
    FILE *fp = fopen("file.txt", "r");
    int freq[26] = {0};
    char c;
    while((c = fgetc(fp)) != EOF){
        if(c >= 'a' && c <= 'z') freq[c - 'a']++;
        else if(c >= 'A' && c <= 'Z') freq[c - 'A']++;
    }
    fclose(fp);

    // Write character frequency to shared memory
    s = shm;
    for(i=0; i<26; i++) *s++ = freq[i];
    *s = '\0';

    // Create child process
    pid_t pid = fork();
    if(pid == -1){
        perror("fork");
        exit(1);
    }
    else if(pid == 0) // Child process
    {
        printf("Child process executing...\n");
        // Attach shared memory segment to child process
        char *shm_child = shmat(shmid, NULL, 0);
        if(shm_child == (char*) -1){
            perror("shmat");
            exit(1);
        }

        // Read character frequency from shared memory and print result
        s = shm_child;
        printf("Character frequency:\n");
        int word[26] = {0}, size = 0;
        for(i=0; i<26; i++){
            word[i] = *s;
            if(word[i]>0) size++;
            printf("%c: %d\n", 'a'+i, *s++);
        }

        printf("\nCompressed Code:\n");
        HuffmanCodes(word, size);

        // Detach shared memory segment from child process
        if(shmdt(shm_child) == -1) {
            perror("shmdt");
            exit(1);
        }
        exit(0);
    }
    else // Parent process
    {
        printf("Parent process executing...\n");

        // Wait for child process to finish
        wait(NULL);

        // Detach shared memory segment from parent process
        if(shmdt(shm) == -1) {
            perror("shmdt");
            exit(1);
        }

        // Deallocate shared memory segment
        if(shmctl(shmid, IPC_RMID, NULL) == -1) {
            perror("shmctl");
            exit(1);
        }
    }
    return 0;
}
