#include<stdio.h>
#include<stdlib.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<pthread.h>
#define COL 5
#define ROW 4
typedef unsigned long long ll;
ll *RESULT_MATRIX;

// Function to calculate factorial of a number
ll findFactorial(ll n){
    if(n == 0) return 1;
    else return n * findFactorial(n-1);
}

// Function to calculate factorial for each element in a row
void calculateFactorial(){
    int i, j;
    for(j=0; j<COL; j++){
        for(i=0; i<ROW; i++){
            ll fac = findFactorial(RESULT_MATRIX[i*COL + j]);
            RESULT_MATRIX[i*COL + j] = fac;
        }
    }
}

int main()
{
    key_t KEY = 2003;
    int SHMID, i, j;

    // Mendapatkan segment shared memory untuk hasil perkalian matriks
    SHMID = shmget(KEY, sizeof(ll)*ROW*COL, 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }


    // Masukkan segment shared memory ke process
    RESULT_MATRIX = (ll*) shmat(SHMID, NULL, 0);
    if(RESULT_MATRIX == (ll*) -1){
        perror("shmat");
        exit(1);
    }


    // Cetak matriks terlebih dahulu
    printf("Result Matrix:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Cukup memanggil Function tanpa Thread Baru
    calculateFactorial();


    // Cetaks Matriks Hasil Faktorial
    printf("Result Matrix Factorial:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Lepas segment shared memory dari process
    if(shmdt(RESULT_MATRIX) == -1){
        perror("shmdt");
        exit(1);
    }


    // Dealokasikan segment shared memory
    if(shmctl(SHMID, IPC_RMID, NULL) == -1){
        perror("shmctl");
        exit(1);
    }

    return 0;
}
