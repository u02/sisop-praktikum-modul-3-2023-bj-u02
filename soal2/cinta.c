#include<stdio.h>
#include<stdlib.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<pthread.h>
#define COL 5
#define ROW 4
typedef unsigned long long ll;
ll *RESULT_MATRIX;

ll findFactorial(ll n){
    if(n == 0) return 1;
    else return n * findFactorial(n-1);
}

void *calculateFactorial(void *args){
    int (*index) = args;
    RESULT_MATRIX[*index] = findFactorial(RESULT_MATRIX[*index]); 

    pthread_exit(NULL);
}

int main()
{
    pthread_t TID[100];
    key_t KEY = 2003;
    int SHMID, i, j;

    // Mendapatkan segment shared memory untuk hasil perkalian matriks
    SHMID = shmget(KEY, sizeof(ll)*ROW*COL, 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }


    // Masukkan segment shared memory ke process
    RESULT_MATRIX = (ll*) shmat(SHMID, NULL, 0);
    if(RESULT_MATRIX == (ll*) -1){
        perror("shmat");
        exit(1);
    }


    // Cetak matriks terlebih dahulu
    printf("Result Matrix:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Menghitung Faktorial dengan Thread Baru
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) {
            int *index = (int*)malloc(sizeof(int));
            *index = i*COL + j;
            pthread_create(&TID[i*COL +j], NULL, calculateFactorial, (void*)index);
        }
        printf("\n");
    }

    // Menunggu Thread selesai
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) {
           if(pthread_join(TID[i*COL +j], NULL)){
            perror("pthread_join");
            exit(1);
            }
        }
    }
    printf("\n");


    // Cetaks Matriks Hasil Faktorial
    printf("Result Matrix Factorial:\n");
    for(i=0; i<ROW; i++){
        for(j=0; j<COL; j++) printf("%llu ", RESULT_MATRIX[i*COL + j]);
        printf("\n");
    }


    // Lepas segment shared memory dari process
    if(shmdt(RESULT_MATRIX) == -1){
        perror("shmdt");
        exit(1);
    }


    // Dealokasikan segment shared memory
    if(shmctl(SHMID, IPC_RMID, NULL) == -1){
        perror("shmctl");
        exit(1);
    }

    return 0;
}
