#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
typedef unsigned long long ll;

int main() {
    key_t key = 2003;
    int mat1[ROW1][COL1], mat2[ROW2][COL2], i, j, k, SHMID;
    ll *result;

    // Inisialisasi untuk random seed
    srand(time(NULL));

    // Isi matriks 1 dengan angka antara 1 sampai dengan 5
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL1; j++) mat1[i][j] = rand() % 5 + 1;
    }

    // Isi matriks 1 dengan angka antara 1 sampai dengan 4
    for(i=0; i<ROW2; i++){
        for(j=0; j<COL2; j++) mat2[i][j] = rand() % 4 + 1;
    }

    // Buat Shared Memory untuk hasil perkalian Matriks 1 & 2
    SHMID = shmget(key, sizeof(ll) * ROW1 * COL2, IPC_CREAT | 0666);
    if(SHMID == -1){
        perror("shmget");
        exit(1);
    }

    // Masukkan segmen dari shared memory ke process
    result = (ll*) shmat(SHMID, NULL, 0);
    if(result == (ll*) -1){
        perror("shmat");
        exit(1);
    }

    // Perkalian matriks sekaligus mencetak hasilnya
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL2; j++){
            result[i*COL2 + j] = 0;
            for(k=0; k<ROW2; k++) result[i*COL2 + j] += mat1[i][k] * mat2[k][j];
            printf("%llu ", result[i*COL2 + j]);
        }
        printf("\n");
    }

    // Melepas shared memory dari process
    if(shmdt(result) == -1){
        perror("shmdt");
        exit(1);
    }

    printf("Result matrix is sent to shared memory.\n");

    return 0;
}
